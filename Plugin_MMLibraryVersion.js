var mmcoreIsPresent = (typeof mmcore === 'object'),
	mmapiIsPresent = (typeof mmsystem == 'object'),
	message;

if (mmcoreIsPresent && mmapiIsPresent) {
	message = 'Migration MMAPI';
} else if (mmcoreIsPresent) {
	message = 'MMCORE';
} else if (mmapiIsPresent) {
	message = 'Original MMAPI';
} else {
	message = 'No library detected';
}

alert(message);